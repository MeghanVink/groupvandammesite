#N frontend

N frontend is a kickstart for all my latest webprojects.  This setup contains the latest CSS helpers, jQuery plugins and basic markup.


###Preview

http://ruttennicky.github.io/n-frontend/


###Gulp
N frontend comes with gulp.

Gulp takes care of:
 - Local webserver (localhost on port 3333)
 - LiveReload
 - JS minify
 - CSS minfy
 - HTML minfy
 - Image optim
 - SASS compiling
 - Watching of changes in HTML & SASS


###Installation instruction
	
1. Install the prerequisite gulp libraries
    
    ```npm install```

2. Just run Gulp
    
    ```gulp serve```

3. Gulp opens Firefox on localhost, port 3333.  Enjoy the auto-reload


###jQuery

Libaries included:
  - jQuery itself
  - Bootstrap
  - Fancybox
  - Slick carousel
  - MMenu
  - Isotope
  - Google Maps API v3
  - Google Analytics snippet


###CSS
Libraries included:
  - Font Awesome (via CDN)
  - Font Awesome amimation (fade & bounce)
  - Hover CSS3 animation
  - Fancybox CSS
  - Slick carousel CSS
  - MMenu CSS
  - Google Webfont (via CDN): Ubuntu & Fauna One ( because these are a pretty fonts :) )


###Questions?

ruttennicky@me.com

###Issues & featues

I use Trello for this: https://trello.com/b/L1Cl4OD7/n-frontend

Feel free to submit requests!
